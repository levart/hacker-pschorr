import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {Actions, ofActionDispatched, Store} from '@ngxs/store';
import {Logout} from './core/state/auth.actions';
import {GlobalAccess, ShippingRatePercent} from './core/state/common.actions';


@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {
  constructor(
    private store: Store,
    private router: Router,
    private actions: Actions
  ) {
  }


  ngOnInit(): void {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
    this.store.dispatch(new ShippingRatePercent());
    this.store.dispatch(new GlobalAccess());
    this.actions.pipe(ofActionDispatched(Logout)).subscribe(() => this.router.navigate(['/']));
  }
}
