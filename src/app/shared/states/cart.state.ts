import {Injectable} from '@angular/core';
import {Action, Selector, State, StateContext, Store} from '@ngxs/store';
import {AddToCart, ConfirmOrder, DeleteCartItem, SearchToggle, SetCardInfoForm, UpdateCartInfo, UpdateCartItem} from './cart.actions';
import {Navigate} from '@ngxs/router-plugin';
import {CartService} from '../../core/services/cart.service';
import {tap} from 'rxjs/operators';
import {NotificationService} from '../../core/services/notification.service';
import {AuthState, AuthStateModel} from '../../core/state/auth.state';
import {Redirect} from '../../core/state/auth.actions';
import {ActivatedRoute, Router} from '@angular/router';


export class CartStateModel {
  cart?: any[];
  cartInfo?: any;
  searchToggle!: boolean;
}


@State<CartStateModel>({
  name: 'carts',
  defaults: {
    cart: [],
    cartInfo: {
      phone: '',
      address: '',
      paymentType: '',
      comment: ''
    },
    searchToggle: false
  }
})
@Injectable()
export class CartState {

  constructor(
    private service: CartService,
    private notificationService: NotificationService,
    private router: Router,
    private store: Store
  ) {
  }


  @Selector()
  static getCartTotalPrice(state: CartStateModel): any {
    return state.cart?.reduce((prev, current) => {
      let boxPrice = 0;
      if (current.product.needBox) {
        boxPrice = current.product.boxPrice;
      }
      return prev + (current.product.price + boxPrice) * current.quantity;
    }, 0);
  }

  @Selector()
  static getCart(state: CartStateModel): any {
    return state.cart;
  }

  @Selector()
  static getCartItem(state: CartStateModel, product: any): any {
    return state.cart?.find(f => f.product.id === product.id);
  }

  @Selector()
  static getCartInfo(state: CartStateModel): any {
    return state.cartInfo;
  }

  @Selector()
  static getCartItemCount(state: CartStateModel): number {
    return <number> state.cart?.length;
  }

  @Selector()
  static getSearchToggle(state: CartStateModel): boolean {
    return state.searchToggle;
  }

  @Action(AddToCart)
  addToCart(ctx: StateContext<CartStateModel>, {payload}: AddToCart): void {
    const {cart} = ctx.getState();
    const findCart = (cart || []).find(f => f.product.id === payload.product.id);
    if (findCart) {
      const findCartIndex = (cart || []).findIndex((f: any) => f.product.id === payload.product.id);
      ctx.patchState({
        cart: [
          ...(cart || []).slice(0, findCartIndex),
          {
            ...findCart,
            quantity: findCart.quantity + 1
          },
          ...(cart || []).slice(findCartIndex + 1)]
      });
    } else {
      ctx.patchState({
        cart: [...(cart || []), payload]
      });
    }

  }

  @Action(UpdateCartItem)
  updateCartItem({patchState, dispatch, getState}: StateContext<CartStateModel>, {payload}: UpdateCartItem): any {
    const {cart} = getState();
    if (payload.quantity === 0) {
      dispatch(new DeleteCartItem(payload));
      return;
    }
    if (!payload.product) {
      return;
    }
    const findCart = (cart || []).find((i: any) => i.product.id === payload.product.id);
    const findIndex = (cart || []).findIndex((i: any) => i.product.id === payload.product.id);
    patchState({
      cart: [
        ...(cart || []).slice(0, findIndex),
        {
          ...findCart,
          quantity: payload.quantity
        },
        ...(cart || []).slice(findIndex + 1),
      ]
    });
  }

  @Action(UpdateCartInfo)
  async updateCartInfo(ctx: StateContext<CartStateModel>): Promise<void> {
    const isAuth = await this.store.selectOnce(AuthState.isAuthenticated).toPromise();
    if (!isAuth) {
      this.store.dispatch(new Redirect(true));
      ctx.dispatch(new Navigate(['/auth']));
      return;
    }
    ctx.dispatch(new Navigate(['/checkout/preview']));
  }

  @Action(ConfirmOrder)
  confirmOrder(ctx: StateContext<CartStateModel>, {payload}: ConfirmOrder): any {
    ctx.patchState({
      cartInfo: payload
    });
    const {cart, cartInfo} = ctx.getState();
    return this.service.orderCreate({cart, cartInfo: payload})
      .pipe(
        tap(res => {
          if (res) {
            ctx.patchState({
              cartInfo: undefined,
              cart: []
            });
            this.notificationService.success('თქვენი შეკვეთა მიღებულია');
            ctx.dispatch(new Navigate(['/']));
          }
        })
      );
  }

  @Action(DeleteCartItem)
  deleteCartItem({patchState, dispatch, getState}: StateContext<CartStateModel>, {payload}: DeleteCartItem): any {
    console.log(payload);
    const {cart} = getState();
    const findIndex = (cart || []).findIndex((i: any) => i.product.id === payload.product.id);
    console.log(findIndex);
    patchState({
      cart: [
        ...(cart || []).slice(0, findIndex),
        ...(cart || []).slice(findIndex + 1),
      ]
    });
  }

  @Action(SetCardInfoForm)
  setCardInfoForm({patchState, dispatch, getState}: StateContext<CartStateModel>, {payload}: SetCardInfoForm): any {
    patchState({
      cartInfo: payload
    });
  }

  @Action(SearchToggle)
  searchToggle(ctx: StateContext<CartStateModel>): void {
    const {searchToggle} = ctx.getState();
    const href = this.router.url;
    if (href !== '/') {
      ctx.dispatch(new Navigate(['/']));
    }
    (function smoothscroll(): void {
      const currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
      if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - (currentScroll / 8));
      }
    })();
    ctx.patchState({
      searchToggle: !searchToggle
    });
  }
}
