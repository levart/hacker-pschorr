import {CartItem} from '../../core/interfaces/cart';

export class AddToCart {
  static readonly type = '[Cart] Add to cart';
  constructor(public payload: {product: any, quantity: number}) {}
}

export class UpdateCartInfo {
  static readonly type = '[Cart] Update Cart Info';
}

export class ConfirmOrder {
  static readonly type = '[Cart] Confirm Order';
  constructor(public payload: any) {}
}


export class SearchToggle {
  static readonly type = '[Cart] SearchToggle';
}

export class DeleteCartItem {
  static readonly type = '[CART] delete to cart item';

  constructor(public payload: CartItem) {
  }
}

export class UpdateCartItem {
  static readonly type = '[CART] update to cart item';

  constructor(public payload: CartItem) {
  }
}

export class SetCardInfoForm {
  static readonly type = '[CART] update to cart item';

  constructor(public payload: any) {
  }
}
