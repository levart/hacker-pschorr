import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductItemComponent} from './components/product-item/product-item.component';
import {RouterModule} from '@angular/router';
import {MatButtonModule} from '@angular/material/button';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {ProductItemV2Component} from './components/product-item-v2/product-item-v2.component';
import {MatDividerModule} from '@angular/material/divider';
import {PriceComponent} from './components/price/price.component';
import {ConfirmDialogComponent} from './components/confirm-dialog/confirm-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {OrderStatusPipe} from './pipes/order-status.pipe';
import {AutofocusDirective} from '../core/directives/autofocus.directive';
import { PaymentTypePipe } from './pipes/payment-type.pipe';


@NgModule({
  declarations: [ProductItemComponent, ProductItemV2Component, PriceComponent, ConfirmDialogComponent, OrderStatusPipe, AutofocusDirective, PaymentTypePipe],
  imports: [
    CommonModule,
    RouterModule,
    MatButtonModule,
    FlexLayoutModule,
    MatIconModule,
    MatDividerModule,
    MatDialogModule
  ],
  exports: [ProductItemComponent, ProductItemV2Component, PriceComponent, ConfirmDialogComponent, OrderStatusPipe, AutofocusDirective, PaymentTypePipe]
})
export class SharedModule {
}
