import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'paymentType'
})
export class PaymentTypePipe implements PipeTransform {

  transform(value: string): string {
    if (value === 'CASH'){
      return 'ნაღდი'
    }
    if (value === 'CARD'){
      return 'ბარათი/ტერმინალი'
    }
    return value;
  }

}
