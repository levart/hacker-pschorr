import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'orderStatus'
})
export class OrderStatusPipe implements PipeTransform {

  transform(value: string): unknown {
    if (value === 'TOBE_ASSIGN') {
      return 'გაგზავნილია დასადასტურებლად';
    }
    if (value === 'ASSIGN') {
      return 'შეკვეთა მზადდება';
    }
    if (value === 'ON_WAY') {
      return 'კურიერი გზაშია';
    }
    if (value === 'COMPLETED') {
      return 'დასრულებული';
    }
    if (value === 'REJECTED') {
      return 'გაუქმებული';
    }
    return value;
  }

}
