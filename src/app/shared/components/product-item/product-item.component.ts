import {Component, Input, OnInit} from '@angular/core';
import {Store} from '@ngxs/store';
import {AddToCart, UpdateCartItem} from '../../states/cart.actions';
import {NotificationService} from '../../../core/services/notification.service';
import {CartState} from '../../states/cart.state';
import {CommonState} from '../../../core/state/common.state';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss'],
})
export class ProductItemComponent implements OnInit {
  @Input() product: any;
  @Input() image: string | null = null;
  @Input() title: string | null = null;
  @Input() description: string | null = null;
  @Input() price: number | string | null = null;

  priceSuf: any;
  pricePref: any;

  quantity = 0;
  disabled: any = false;

  constructor(
    private store: Store,
    private notificationService: NotificationService,
  ) {
  }

  ngOnInit(): void {
    this.store.select(({carts}) => CartState.getCartItem(carts, this.product))
      .subscribe(res => {
        console.log(res);
        if (res) {
          this.quantity = res.quantity;
        }
      });
    this.store.select(CommonState.getGlobalAccess)
      .subscribe(res => {
        if (res) {
          this.disabled = res;
        }
      });
    this.setPrice();
  }

  setPrice(): void {
    const splited = this.price?.toString().split('.');
    if (splited && splited.length) {
      this.priceSuf = splited[0];
      this.pricePref = splited[1];
    }
  }


  decrement(): void {
    this.quantity--;
    if (this.quantity <= 0) {
      this.quantity = 0;
    }
    this.store.dispatch(new UpdateCartItem({product: this.product, quantity: this.quantity}));
  }

  increment(): void {
    if (this.disabled) {
      this.notificationService.info('შეკვეთის მიღება დროებით შეჩერებულია');
      return;
    }
    this.quantity++;
    this.store.dispatch(new AddToCart({product: this.product, quantity: this.quantity}));
    this.notificationService.success(`${this.product.name} დაემატა შეკვეთაში`);
  }

  addToCart(): void {
    this.store.dispatch(new AddToCart({product: this.product, quantity: this.quantity}));
  }
}
