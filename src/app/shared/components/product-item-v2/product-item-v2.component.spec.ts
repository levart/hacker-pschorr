import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductItemV2Component } from './product-item-v2.component';

describe('ProductItemV2Component', () => {
  let component: ProductItemV2Component;
  let fixture: ComponentFixture<ProductItemV2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductItemV2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductItemV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
