import {Component, Input, OnInit} from '@angular/core';
import {Store} from '@ngxs/store';
import {AddToCart} from '../../states/cart.actions';

@Component({
  selector: 'app-product-item-v2',
  templateUrl: './product-item-v2.component.html',
  styleUrls: ['./product-item-v2.component.scss']
})
export class ProductItemV2Component implements OnInit {

  @Input() image: string | null = null;
  @Input() title: string | null = null;
  @Input() price: number | string | null = null;

  priceSuf: any;
  pricePref: any;

  quantity = 1;

  constructor(
    private store: Store
  ) {
  }

  ngOnInit(): void {
    this.setPrice();
  }

  setPrice(): void {
    const splited = this.price?.toString().split('.');
    if (splited && splited.length) {
      this.priceSuf = splited[0];
      this.pricePref = splited[1];
    }


  }


  decrement(): void {
    this.quantity--;
    if (this.quantity <= 0) {
      this.quantity = 1;
    }
  }

  increment(): void {
    this.quantity++;
  }

  addToCart(): void{
    this.store.dispatch(new AddToCart({product: {}, quantity: this.quantity}));
  }

}
