import {ChangeDetectionStrategy, Component, HostBinding, Input, OnInit} from '@angular/core';
import {formatNumber} from '@angular/common';
import {DomSanitizer, SafeStyle} from '@angular/platform-browser';

@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PriceComponent implements OnInit {
  @Input() price: number | string = 0;

  priceSuf: any;
  pricePref: any;



  constructor() {}

  ngOnInit(): void {
    this.setPrice();
  }

  setPrice(): void {
    this.price = formatNumber(+this.price || 0, 'en-US', '1.2-2');
    const splited = this.price?.toString().split('.');
    if (splited && splited.length) {
      this.priceSuf = splited[0];
      this.pricePref = splited[1];
    }
  }

}
