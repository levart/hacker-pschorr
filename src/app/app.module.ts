import {NgModule} from '@angular/core';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {MatIconModule, MatIconRegistry} from '@angular/material/icon';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LayoutModule} from './layout/layout.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {FlexLayoutModule} from '@angular/flex-layout';
import {CartState} from './shared/states/cart.state';
import {NgxsModule} from '@ngxs/store';
import {NgxsReduxDevtoolsPluginModule} from '@ngxs/devtools-plugin';
import {NgxsRouterPluginModule} from '@ngxs/router-plugin';
import {AuthState} from './core/state/auth.state';
import {ToastrModule} from 'ngx-toastr';
import {NgxsStoragePluginModule, StorageOption} from '@ngxs/storage-plugin';
import {HttpErrorInterceptor} from './core/interceptors/http-error.interceptor';
import {TokenInterceptor} from './core/interceptors/token.interceptor';
import {OrdersComponent} from './pages/orders/orders.component';
import {MatDividerModule} from '@angular/material/divider';
import {CommonState} from './core/state/common.state';
import {SharedModule} from './shared/shared.module';
import {NgxsFormPluginModule} from '@ngxs/form-plugin';

@NgModule({
  declarations: [
    AppComponent,
    OrdersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    RouterModule,
    HttpClientModule,
    LayoutModule,
    MatIconModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    FlexLayoutModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-top-center',
      preventDuplicates: true,
      countDuplicates: true,
      maxOpened: 2,
      autoDismiss: true
    }),
    NgxsModule.forRoot([AuthState, CartState, CommonState], {developmentMode: !environment.production}),
    NgxsStoragePluginModule.forRoot({
      key: ['auth.token', 'auth.user', 'auth.isAuthenticated', 'auth.forCart', 'carts.cart', 'carts.cartInfo', 'common.globalOrderAccess', 'common.shippingRatePercent'],
      storage: StorageOption.SessionStorage
    }),
    NgxsReduxDevtoolsPluginModule.forRoot({disabled: environment.production}),
    NgxsRouterPluginModule.forRoot(),
    NgxsFormPluginModule.forRoot(),
    MatDividerModule,
    SharedModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(iconRegistry: MatIconRegistry, domSanitizer: DomSanitizer) {
    iconRegistry.addSvgIconSet(
      domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi.svg')
    );
  }
}
