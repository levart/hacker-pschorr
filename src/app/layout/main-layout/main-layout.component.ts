import {Component, OnInit, ViewChild} from '@angular/core';
import {Store} from '@ngxs/store';
import {Logout} from '../../core/state/auth.actions';
import {MatDrawer} from '@angular/material/sidenav';
import {Router} from '@angular/router';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {
  @ViewChild('menu')
  menu!: MatDrawer;

  constructor(
    private store: Store,
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  handleSearch(): void {
    console.log('search clicked');
  }

  logout(): void {
    this.store.dispatch(new Logout());
    this.menu.toggle();
  }

  handleMenuOpen($event: any): void {
    this.menu.toggle();
  }

  handleMenuRoute(s: string): void {
    this.router.navigate(['/' + s]).then();
    this.menu.toggle().then();

  }
}
