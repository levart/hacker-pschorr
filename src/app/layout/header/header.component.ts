import {Component, OnInit} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Select, Store} from '@ngxs/store';
import {CartState} from '../../shared/states/cart.state';
import {Observable} from 'rxjs';
import {GlobalAccess} from '../../core/state/common.actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Select(CartState.getCartItemCount) cartCount$: Observable<number> | undefined;

  isSmallScreen: boolean = false;

  constructor(
    private store: Store,
    breakpointObserver: BreakpointObserver
  ) {
    breakpointObserver.observe([
      Breakpoints.HandsetLandscape,
      Breakpoints.HandsetPortrait
    ]).subscribe(result => {
      if (result.matches) {
        this.isSmallScreen = breakpointObserver.isMatched('(max-width: 768px)');
      }
    });
  }

  ngOnInit(): void {

  }

}
