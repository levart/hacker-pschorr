import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {CartState} from '../../shared/states/cart.state';
import {Observable, of} from 'rxjs';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {SearchToggle} from '../../shared/states/cart.actions';
import {AuthState} from '../../core/state/auth.state';
import {IUser} from '../../core/interfaces/user.model';
import {CommonState} from '../../core/state/common.state';
import {Navigate} from '@ngxs/router-plugin';
import {NotificationService} from '../../core/services/notification.service';

@Component({
  selector: 'app-header-v2',
  templateUrl: './header-v2.component.html',
  styleUrls: ['./header-v2.component.scss']
})
export class HeaderV2Component implements OnInit {
  @Select(AuthState.isAuthenticated)
  isAuthenticated$: Observable<boolean> | undefined;

  @Select(AuthState.getProfile)
  profile$!: Observable<IUser>;

  @Select(CartState.getCartItemCount)
  cartCount$!: Observable<number>;

  cartCount: number = 0;

  @Output() menuClicked = new EventEmitter();
  @Output() cartClicked = new EventEmitter();
  @Output() searchClicked = new EventEmitter();

  disabled = false;
  menuClick = false;

  isSmallScreen: boolean = false;

  constructor(
    breakpointObserver: BreakpointObserver,
    private store: Store,
    private notificationService: NotificationService,
  ) {
    breakpointObserver.observe([
      Breakpoints.HandsetLandscape,
      Breakpoints.HandsetPortrait
    ]).subscribe(result => {
      if (result.matches) {
        this.isSmallScreen = breakpointObserver.isMatched('(max-width: 768px)');
      }
    });
    this.store.select(CartState.getCartItemCount)
      .subscribe(res => {
        this.cartCount = res;
      });
  }

  ngOnInit(): void {
    this.store.select(CommonState.getGlobalAccess)
      .subscribe(res => {
        if (res) {
          this.disabled = res;
        }
      });
  }


  profileToggle(): void {
    this.menuClick = !this.menuClick;
    this.menuClicked.emit(this.menuClick);
  }

  cartToggle(): void {
    this.cartClicked.emit();
  }

  handleSearch(): void {
    this.store.dispatch(new SearchToggle());
  }

  handleCheckout(): void {
    if(this.cartCount <= 0 ) {
      this.store.dispatch(new Navigate(['/']));
      return;
    };

    if (this.disabled) {
      this.notificationService.info('შეკვეთის მიღება დროებით შეჩერებულია');
      return;
    }
    this.store.dispatch(new Navigate(['/checkout']));
  }
}
