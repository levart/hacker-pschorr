import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainLayoutComponent} from './main-layout/main-layout.component';
import {RouterModule} from '@angular/router';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {MatIconModule} from '@angular/material/icon';
import {MatBadgeModule} from '@angular/material/badge';
import {ExtendedModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {HeaderV2Component} from './header-v2/header-v2.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatDividerModule} from '@angular/material/divider';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  declarations: [MainLayoutComponent, HeaderComponent, FooterComponent, HeaderV2Component],
  imports: [
    CommonModule,
    RouterModule,
    MatIconModule,
    MatBadgeModule,
    ExtendedModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatDividerModule,
    MatIconModule,
    SharedModule
  ],
  exports: [MainLayoutComponent, HeaderComponent, FooterComponent, HeaderV2Component]
})
export class LayoutModule {
}
