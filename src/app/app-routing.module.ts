import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainLayoutComponent} from './layout/main-layout/main-layout.component';
import {MainModule} from './pages/main/main.module';
import {ProductsModule} from './pages/products/products.module';
import {ProductDetailModule} from './pages/product-detail/product-detail.module';
import {AuthModule} from './pages/auth/auth.module';
import {CheckoutModule} from './pages/checkout/checkout.module';
import {OrdersComponent} from './pages/orders/orders.component';
import {ProfileModule} from './pages/profile/profile.module';
import {AuthGuard} from './core/guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [

      {
        path: '',
        loadChildren: () => MainModule
      },
      {
        path: 'auth',
        loadChildren: () => AuthModule
      },
      {
        path: 'products',
        loadChildren: () => ProductsModule
      },
      {
        path: 'product',
        loadChildren: () => ProductDetailModule
      },
      {
        path: 'checkout',
        loadChildren: () => CheckoutModule
      },
      {
        path: 'orders',
        canActivate: [AuthGuard],
        component: OrdersComponent
      },
      {
        path: 'profile',
        canActivate: [AuthGuard],
        loadChildren: () => ProfileModule
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
