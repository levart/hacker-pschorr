import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductDetailRoutingModule } from './product-detail-routing.module';
import { ProductDetailComponent } from './product-detail.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  declarations: [ProductDetailComponent],
    imports: [
        CommonModule,
        ProductDetailRoutingModule,
        FlexLayoutModule,
        MatButtonModule,
        MatIconModule
    ]
})
export class ProductDetailModule { }
