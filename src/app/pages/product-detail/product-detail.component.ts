import { Component, OnInit } from '@angular/core';
import {Store} from '@ngxs/store';
import {AddToCart} from '../../shared/states/cart.actions';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  priceSuf: any;
  pricePref: any;

  quantity = 1;

  constructor(
    private store: Store
  ) {
  }

  ngOnInit(): void {
    this.setPrice();
  }

  setPrice(): void {
    const splited = 15.99.toString();
    if (splited && splited.length) {
      this.priceSuf = splited[0];
      this.pricePref = splited[1];
    }


  }


  decrement(): void {
    this.quantity--;
    if (this.quantity <= 0) {
      this.quantity = 1;
    }
  }

  increment(): void {
    this.quantity++;
  }

  addToCart(): void{
    this.store.dispatch(new AddToCart({product: {}, quantity: this.quantity}));
  }

}
