import {Component, OnInit} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {CartState} from '../../../../shared/states/cart.state';
import {Observable} from 'rxjs';
import {UpdateCartInfo} from '../../../../shared/states/cart.actions';
import {AuthState} from '../../../../core/state/auth.state';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  @Select(CartState.getCart) carts$!: Observable<any>;

  constructor(private store: Store) {
  }

  ngOnInit(): void {

  }

  orderCreate(): void {
    this.store.dispatch(new UpdateCartInfo());
  }
}
