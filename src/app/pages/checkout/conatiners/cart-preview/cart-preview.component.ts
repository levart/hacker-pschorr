import {Component, OnInit} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {CartState} from '../../../../shared/states/cart.state';
import {Observable} from 'rxjs';
import {ConfirmOrder, SetCardInfoForm, UpdateCartInfo} from '../../../../shared/states/cart.actions';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CommonState} from '../../../../core/state/common.state';
import {AuthState} from '../../../../core/state/auth.state';
import {MatRadioChange} from '@angular/material/radio';
import {UpdateFormValue} from '@ngxs/form-plugin';

@Component({
  selector: 'app-cart-preview',
  templateUrl: './cart-preview.component.html',
  styleUrls: ['./cart-preview.component.scss']
})
export class CartPreviewComponent implements OnInit {
  @Select(CartState.getCart) carts$!: Observable<any>;
  @Select(CartState.getCartInfo) cartInfo$!: Observable<any>;

  form: FormGroup;
  totalPrice: number = 0;
  shippingRate: number = 0;

  constructor(
    private store: Store
  ) {
    this.form = new FormGroup({
      phone: new FormControl('', Validators.compose([Validators.required, Validators.minLength(9), Validators.maxLength(9)])),
      address: new FormControl('', Validators.required),
      paymentType: new FormControl('', Validators.required),
      comment: new FormControl(''),
    });

  }

  ngOnInit(): void {
    this.store.select(CartState.getCartInfo)
      .subscribe(res => {
        if (res) {
          this.form.patchValue(res);
        }
      });
    this.store.select(AuthState.getProfile)
      .subscribe(res => {
        console.log(res);
        if (res) {
          this.form.patchValue({
            phone: res.phone,
            address: res.address,
          });
          this.store.dispatch(new SetCardInfoForm(this.form.value))
        }
      });
    this.store.select(CartState.getCartTotalPrice)
      .subscribe(res => {
        if (res) {
          this.totalPrice = res;
        }
      });
    this.store.select(CommonState.getShippingRatePercent)
      .subscribe(res => {
        if (res) {
          this.shippingRate = res;
        }
      });
  }

  orderConfirm(): void {
    this.form.markAllAsTouched();
    if (this.form.invalid) {
      return;
    }
    this.store.dispatch(new ConfirmOrder(this.form.value));
  }

  changeForm($event: any): void {
    this.store.dispatch(new SetCardInfoForm(this.form.value))
  }
}
