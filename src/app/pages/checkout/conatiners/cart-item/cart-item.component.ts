import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AddToCart, DeleteCartItem, UpdateCartItem} from '../../../../shared/states/cart.actions';
import {Store} from '@ngxs/store';
import {ConfirmDialogComponent} from '../../../../shared/components/confirm-dialog/confirm-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss']
})
export class CartItemComponent implements OnInit {
  @Input() cart: any;
  @Input() quantity: number = 2;
  @Output() refreshCart = new EventEmitter();
  disabled: any = true;
  apiUrl = environment.apiUrl;

  constructor(
    private store: Store,
    private dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
  }

  decrement(): void {
    this.quantity--;
    if (this.quantity <= 0) {
      this.quantity = 0;
    }
    this.store.dispatch(new UpdateCartItem({...this.cart, quantity: this.quantity}));
  }

  increment(): void {
    this.quantity++;
    this.store.dispatch(new AddToCart({...this.cart, quantity: this.quantity}));
  }

  delete(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '300px',
      data: {title: 'წაშლა', content: 'ნამდვილად გსურთ წაშლა?'}
    });
    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.store.dispatch(new DeleteCartItem(this.cart));
      }
    });

  }
}
