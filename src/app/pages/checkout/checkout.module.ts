import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CheckoutRoutingModule} from './checkout-routing.module';
import {CartComponent} from './conatiners/cart/cart.component';
import {CartItemComponent} from './conatiners/cart-item/cart-item.component';
import {MatDividerModule} from '@angular/material/divider';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CartPreviewComponent } from './conatiners/cart-preview/cart-preview.component';
import {MatIconModule} from '@angular/material/icon';
import {SharedModule} from '../../shared/shared.module';
import {NgxsFormPluginModule} from '@ngxs/form-plugin';


@NgModule({
  declarations: [
    CartComponent,
    CartItemComponent,
    CartPreviewComponent
  ],
    imports: [
        CommonModule,
        CheckoutRoutingModule,
        MatDividerModule,
        FlexLayoutModule,
        MatFormFieldModule,
        MatInputModule,
        MatRadioModule,
        MatButtonModule,
        FormsModule,
        ReactiveFormsModule,
        MatIconModule,
        SharedModule,
      NgxsFormPluginModule
    ]
})
export class CheckoutModule {
}
