import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CartComponent} from './conatiners/cart/cart.component';
import {CartPreviewComponent} from './conatiners/cart-preview/cart-preview.component';

const routes: Routes = [
  {
    path: '',
    component: CartComponent
  },
  {
    path: 'preview',
    component: CartPreviewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckoutRoutingModule {
}
