import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileEditComponent } from './containers/profile-edit/profile-edit.component';
import {SharedModule} from '../../shared/shared.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { PasswordUpdateComponent } from './containers/password-update/password-update.component';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  declarations: [ProfileEditComponent, PasswordUpdateComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    SharedModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule
  ]
})
export class ProfileModule { }
