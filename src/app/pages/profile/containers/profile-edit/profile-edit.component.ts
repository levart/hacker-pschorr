import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngxs/store';
import {ConfirmedValidator} from '../../../../core/validators/confirmed.validator';
import {Register} from '../../../../core/state/auth.actions';
import {UserService} from '../../../../core/services/user.service';
import {NotificationService} from '../../../../core/services/notification.service';
import {AuthState} from '../../../../core/state/auth.state';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss']
})
export class ProfileEditComponent implements OnInit {
  form: FormGroup;

  get f(): any {
    return this.form.controls;
  }

  constructor(
    private store: Store,
    private userService: UserService,
    private notificationService: NotificationService,
    private fb: FormBuilder
  ) {
    this.form = fb.group({
      email: ['', [Validators.required]],
      fullName: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      address: ['']
    });
  }

  ngOnInit(): void {
    this.store.select(AuthState.getProfile).subscribe(user => {
      this.form.patchValue(user);
    });
  }

  submit(): void {
    if (this.form.invalid) {
      return;
    }
    this.userService.update(this.form.value)
      .subscribe(res => {
        if (res) {
          this.notificationService.success('პროფილი განახლდა');
        }
      });
  }

}
