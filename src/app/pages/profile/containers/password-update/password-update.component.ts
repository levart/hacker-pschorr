import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngxs/store';
import {ConfirmedValidator} from '../../../../core/validators/confirmed.validator';
import {Register} from '../../../../core/state/auth.actions';
import {UserService} from '../../../../core/services/user.service';
import {NotificationService} from '../../../../core/services/notification.service';

@Component({
  selector: 'app-password-update',
  templateUrl: './password-update.component.html',
  styleUrls: ['./password-update.component.scss']
})
export class PasswordUpdateComponent implements OnInit {

  form: FormGroup;
  password = true;
  confirmPassword = true;

  get f(): any{
    return this.form.controls;
  }
  constructor(
    private store: Store,
    private userService: UserService,
    private notificationService: NotificationService,
    private fb: FormBuilder
  ) {
    this.form = fb.group({
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]]
    }, {
      validator: ConfirmedValidator('password', 'confirmPassword')
    });
  }

  ngOnInit(): void {
  }

  submit(): void {
    if (this.form.invalid) {
      return;
    }

    this.userService.passwordUpdate(this.form.value)
      .subscribe(res => {
        if (res) {
          this.notificationService.success('პაროლი განახლდა');
        }
      });

  }

}
