import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProfileEditComponent} from './containers/profile-edit/profile-edit.component';
import {PasswordUpdateComponent} from './containers/password-update/password-update.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileEditComponent
  },
  {
    path: 'passwordUpdate',
    component: PasswordUpdateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
