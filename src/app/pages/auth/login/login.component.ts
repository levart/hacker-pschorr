import {Component, OnInit} from '@angular/core';
import {Store} from '@ngxs/store';
import {Login} from '../../../core/state/auth.actions';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  password = true;

  constructor(
    private store: Store,
    private fb: FormBuilder
  ) {
    this.form = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, Validators.required],
    });
  }

  ngOnInit(): void {
  }

  submit(): void {
    if (this.form.invalid) {
      return;
    }
    this.store.dispatch(new Login(this.form.value))
  }

}
