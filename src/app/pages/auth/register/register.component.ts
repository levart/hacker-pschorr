import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {AuthService} from '../../../core/services/auth.service';
import {Store} from '@ngxs/store';
import {Register} from '../../../core/state/auth.actions';
import {ConfirmedValidator} from '../../../core/validators/confirmed.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;
  password = true;
  confirmPassword = true;

  get f(): any{
    return this.form.controls;
  }
  constructor(
    private store: Store,
    private fb: FormBuilder
  ) {
    this.form = fb.group({
      email: ['', [Validators.required]],
      fullName: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]]
    }, {
      validator: ConfirmedValidator('password', 'confirmPassword')
    });
  }

  ngOnInit(): void {
  }

  submit(): void {
    console.log(this.form);
    if (this.form.invalid) {
      return;
    }

    this.store.dispatch(new Register(this.form.value));

  }
}
