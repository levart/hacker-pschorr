import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  category: string | null;

  constructor(
    private readonly route: ActivatedRoute
  ) {
    this.category = null;
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.category = params.category;
    });
  }

}
