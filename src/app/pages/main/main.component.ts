import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Select} from '@ngxs/store';
import {CartState} from '../../shared/states/cart.state';
import {Observable} from 'rxjs';
import {ProductService} from '../../core/services/product.service';
import {environment} from '../../../environments/environment';
import {debounceTime, delay, filter, map, tap} from 'rxjs/operators';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  @Select(CartState.getSearchToggle) searchState$: Observable<boolean> | undefined;
  slides = [
    {image: 'https://gsr.dev/material2-carousel/assets/demo.png'},
    {image: 'https://gsr.dev/material2-carousel/assets/demo.png'},
    {image: 'https://gsr.dev/material2-carousel/assets/demo.png'},
    {image: 'https://gsr.dev/material2-carousel/assets/demo.png'},
    {image: 'https://gsr.dev/material2-carousel/assets/demo.png'}
  ];
  value = null;

  apiUrl = environment.apiUrl;

  products$!: Observable<any>;
  loader = false;

  constructor(
    private productService: ProductService,
    private spinner: NgxSpinnerService
  ) {
  }

  ngOnInit(): void {
    this.searchState$?.subscribe(res => {
      (function smoothscroll(): void {
        const currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
        if (currentScroll > 0) {
          window.requestAnimationFrame(smoothscroll);
          window.scrollTo(0, currentScroll - (currentScroll / 8));
        }
      })();

    });
    this.getProducts();
  }

  getProducts(search = null): void {

    this.products$ = this.productService.get();
    if (search) {
      this.products$ = this.products$.pipe(
        debounceTime(500),
        map(results => results.filter((product: any) => product.name.toString().includes(search))),
        delay(300)
      );
    } else {
      this.products$ = this.products$.pipe(
        delay(300)
      );
    }

  }

  search($event: any): void {
    this.getProducts($event);
  }

  closeSearch(): void {
    this.value = null;
    this.getProducts();
  }
}
