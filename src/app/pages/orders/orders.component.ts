import {Component, OnInit} from '@angular/core';
import {OrderService} from '../../core/services/order.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  orders$!: Observable<any>;

  orders: any[] = [];

  constructor(
    private orderService: OrderService
  ) {
  }

  ngOnInit(): void {
    this.getOrders();
  }

  getOrders(): void {
    this.orderService.getOrders({})
      .subscribe(res => {
        this.orders = res.data;
      });
  }

  orderStatusColor(status: string): string {
    console.log(status);
    switch (status) {
      case 'ASSIGN':
        return 'rgb(225 174 29)';
      case 'TOBE_ASSIGN':
        return 'rgb(12 109 247 / 65%)';
      case 'COMPLETED':
        return 'green';
      case 'ON_WAY':
        return 'rgb(225 174 29)';
      case 'REJECTED':
        return 'red';
      default:
        return 'rgba(225, 174, 29, 0.5)';
    }
  }
}
