import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of, Subject, throwError} from 'rxjs';
import {IApiResponse} from '../interfaces/api-response.model';
import {catchError, concatMap} from 'rxjs/operators';
import {isArray} from 'rxjs/internal-compatibility';
import {environment} from '../../../environments/environment';

@Injectable()
export class HttpWrapper {
  private get baseUrl(): string {
    return environment.apiUrl;
  }

  private loadingCount = 0;
  private isLoading = false;
  private loadingChangedSource = new Subject<any>();
  loadingChanged = this.loadingChangedSource.asObservable();

  constructor(private http: HttpClient) {
  }

  public getBlob(url: string, filename = '', params = {}): void {
    const httpParams = this.getHttpParams(params);
    this.http
      .get(this.baseUrl + url, {
        observe: 'body',
        responseType: 'blob',
        params: httpParams
      })
      .subscribe(response => {
        const dataType = response.type;
        const binaryData = [];
        binaryData.push(response);
        const downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));
        if (filename) {
          downloadLink.setAttribute('download', filename);
        }
        document.body.appendChild(downloadLink);
        downloadLink.click();
      });
  }

  postBlob(url: string, filename = '', params = {}): void {
    this.http
      .post(this.baseUrl + url, params, {
        observe: 'body',
        responseType: 'blob'
      })
      .subscribe(response => {
        const dataType = response.type;
        const binaryData = [];
        binaryData.push(response);
        const downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));
        if (filename) {
          downloadLink.setAttribute('download', filename);
        }
        document.body.appendChild(downloadLink);
        downloadLink.click();
      });
  }

  public get<T>(url: string, parameters?: any): Observable<any> {
    const options = {
      params: this.getHttpParams(parameters)
    };
    return this.http.get<IApiResponse<T>>(`${this.baseUrl}${url}`, options).pipe(
      concatMap(res => {
        return of(res);
      }),
      catchError((err) => throwError(err))
    );
  }

  public post<T>(url: string, options?: any): Observable<any> {
    return this.http.post<T>(`${this.baseUrl}${url}`, options, {observe: 'response'}).pipe(
      concatMap(res => {
        if (res.ok) {
          if (!res.body && res.status === 200) {
            return of(true);
          }
          return of(res.body);
        } else {
          throw res.body;
        }
      }),
      catchError((err) => throwError(err))
    );
  }

  public put<T>(url: string, options?: any): Observable<any> {
    return this.http.put<T>(`${this.baseUrl}${url}`, options, {observe: 'response'}).pipe(
      concatMap(res => {
        if (res.ok) {
          if (!res.body && res.status === 200) {
            return of(true);
          }
          return of(res.body);
        } else {
          throw res.body;
        }
      }),
      catchError((err) => throwError(err))
    );
  }

  public delete<T>(url: string, options?: any): Observable<any> {
    return this.http.delete<T>(`${this.baseUrl}${url}`, {observe: 'response'}).pipe(
      concatMap(res => {
        if (res.ok) {
          if (!res.body && res.status === 200) {
            return of(true);
          }
          return of(res.body);
        } else {
          throw res.body;
        }
      }),
      catchError((err) => throwError(err))
    );
  }

  private getHttpParams(parameters: any): any {
    let httpParams = new HttpParams();

    function encodeValue(value: any): any {
      if (value && value.toString() === '[object Object]') {
        return JSON.stringify(value);
      } else {
        return value;
      }
    }

    for (const key in parameters) {
      if (!parameters.hasOwnProperty(key)) {
        continue;
      }
      const val = parameters[key];

      if (isArray(val)) {
        val.forEach(element => {
          httpParams = httpParams.append(key, encodeValue(element));
        });
      } else {
        httpParams = httpParams.append(key, encodeValue(val));
      }
    }
    return httpParams;
  }

}
