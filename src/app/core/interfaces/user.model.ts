export interface IUser {
  id: string;
  firstName: string;
  lastName: string;
  fullName: string;
  role: string;
  username: string;
  email: string;
  dob: Date;
  gender: string;
  phoneNumber: string;
  mobileNumber: string;
  identityNumber: string;
  city: string;
  address: string;
  address2: string;
  smsBalance: number;
}
