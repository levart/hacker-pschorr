import {City} from './city';
import {User} from './user';
import {Country} from './country';

export interface Address {
  id: string;
  branchId?: any;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: any;
  userId: string;
  cityId: number;
  region: string;
  zipCode: string;
  countryId?: any;
  address: string;
  address2: string;
  recipientName: string;
  recipientNumber: string;
  additionalInformation: string;
  isMain: boolean;
  type: string;
  user: User;
  country: Country;
  city: City;
}
