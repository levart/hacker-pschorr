export interface City {
  id: number;
  cityId: number;
  name: string;
}
