export interface Meta {
  page: number;
  pageSize: number;
  itemCount: number;
  pageCount: number;
}

export interface PaginatedResponse<T> {
  data: T[];
  meta: Meta;
}
