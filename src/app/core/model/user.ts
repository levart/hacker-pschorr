export interface User {
  id: string;
  branchId?: any;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: any;
  firstName: string;
  lastName: string;
  fullName: string;
  mobileNumber: string;
  gender: string;
  identityNumber?: any;
  roles: string;
  birthDate?: any;
  email: string;
  city?: any;
  address?: any;
  address2?: any;
}
