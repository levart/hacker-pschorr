export interface PaginatedRequest {
  page: number;
  pageSize: number;
  order: string;
  s: string;
}

export class PaginatedRequestImpl implements PaginatedRequest {
  order: string = 'DESC';
  page: number = 1;
  pageSize: number = 10;
  s: string = null;
}

