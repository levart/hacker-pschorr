export interface Sender {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: any;
  name: string;
  userId: string;
  status: string;
  agreement: string;
  agreementDate?: any;
}
