import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {first} from 'rxjs/operators';
import {Store} from '@ngxs/store';
import {flatMap} from 'rxjs/internal/operators';
import {CommonState} from '../state/common.state';
import {Injectable} from '@angular/core';

@Injectable()
export class L18nInterceptor implements HttpInterceptor {
  constructor(private store: Store) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.store.select(CommonState.getL18N).pipe(
      first(),
      flatMap((l18n: { lang: string, currency: string }) => {
        return next.handle(req.clone({
          setHeaders: {Lang: l18n.lang, Currency: l18n.currency}
        }));
      })
    );
  }
}
