import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {Store} from '@ngxs/store';
import {AuthState} from '../state/auth.state';
import {Navigate} from '@ngxs/router-plugin';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private store: Store) {
  }

  canActivate(): boolean {
    const isAuthenticated = this.store.selectSnapshot(AuthState.isAuthenticated);
    if (!isAuthenticated) {
      this.store.dispatch(new Navigate(['/'], undefined, {fragment: 'login'}));
      return false;
    }
    return true;
  }

}
