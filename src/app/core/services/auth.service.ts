import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HttpWrapper} from '../http-wrapper/http-wrapper';
import {IAuth, ILogin, IRegister} from '../interfaces/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends HttpWrapper {

  constructor(httpClient: HttpClient) {
    super(httpClient);
  }

  register(auth: IRegister): Observable<any> {
    return super.post('auth/register', auth);
  }

  login(auth: ILogin): Observable<IAuth> {
    return super.post('auth/login/mobile', auth);
  }

  recovery(auth: {email: string}): Observable<IAuth> {
    return super.post('auth/recovery/mobile', auth);
  }


  refreshToken(refreshToken: string): Observable<any> {
    return super.post('auth/token', {refreshToken});
  }
}
