import {Injectable} from '@angular/core';
import {HttpWrapper} from '../http-wrapper/http-wrapper';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http);
  }

  get(params = {}): Observable<any> {
    return super.get('cart', params);
  }

  getOrderAccess(): Observable<any> {
    return super.get('api/getOrderAccess');
  }
  shippingRatePercent(): Observable<any> {
    return super.get('api/shippingRatePercent');
  }

  orderCreate(params = {}): Observable<any> {
    return super.post('api/addOrder', params);
  }

  addToCart(product: any): Observable<any> {
    return super.post(`cart`, product);
  }

  getCartItems(): Observable<any[]> {
    return super.get('cart/me');
  }

  getShippingPrice(params = {}): Observable<any[]> {
    return super.post('cart/shippingPrice', params);
  }
}
