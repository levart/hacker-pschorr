import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HttpWrapper} from '../http-wrapper/http-wrapper';
import {IAuth, ILogin, IRegister} from '../interfaces/auth';

@Injectable({
  providedIn: 'root'
})
export class UserService extends HttpWrapper {

  constructor(httpClient: HttpClient) {
    super(httpClient);
  }

  update(params: any): Observable<any> {
    return super.post('users/update', params);
  }
  passwordUpdate(params: any): Observable<any> {
    return super.post('users/passwordUpdate', params);
  }
}
