import {Injectable} from '@angular/core';
import {HttpWrapper} from '../http-wrapper/http-wrapper';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http);
  }

  get(params= {}): Observable<any> {
    return super.get('api/getProducts', params);
  }
}
