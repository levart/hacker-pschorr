import { Injectable } from '@angular/core';
import {HttpWrapper} from '../http-wrapper/http-wrapper';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PaginatedResponse} from '../model/paginated-response';
import {Sender} from '../model/sender';

@Injectable({
  providedIn: 'root'
})
export class ProductCategoryService  extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http);
  }
  post(options?: any): Observable<any> {
    return super.post('api/brand', options);
  }

  put(options?: any): Observable<any> {
    return super.put('api/brand', options);
  }

  delete(options?: any): Observable<any> {
    return super.delete('api/brand', options);
  }

  get(): Observable<any> {
    return super.get('api/productCategories');
  }

  getBySlug(slug): Observable<any> {
    return super.get('api/productCategorySlug',{slug});
  }

  getSizes(): Observable<any> {
    return super.get('api/getSizes');
  }

  getGender(): Observable<any> {
    return super.get('api/getGender');
  }
}
