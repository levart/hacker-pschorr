import {Directive, ElementRef, Input, OnInit} from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[autofocus]'
})
export class AutofocusDirective implements OnInit {

  private autofocusP: boolean = true;

  constructor(private el: ElementRef) {
  }

  ngOnInit(): void {
    if (this.autofocusP || typeof this.autofocusP === 'undefined') {
      this.el.nativeElement.focus();
    }
  }

  @Input() set autofocus(condition: boolean) {
    this.autofocusP = condition;
  }

}
