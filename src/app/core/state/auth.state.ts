import {Navigate} from '@ngxs/router-plugin';
import {Action, Selector, State, StateContext} from '@ngxs/store';
import {tap} from 'rxjs/operators';
import {Auth, Login, LoginRedirect, Logout, Recovery, Redirect, RefreshToken, Register, SetUserId} from './auth.actions';

import {Injectable} from '@angular/core';
import {AuthService} from '../services/auth.service';
import {IAuth, Token} from '../interfaces/auth';
import {IUser} from '../interfaces/user.model';
import {NotificationService} from '../services/notification.service';

export class AuthStateModel {
  isAuthenticated?: boolean;
  token?: Token | null;
  user?: IUser;
  userId?: string;
  forCart?: string;
  redirectAfterLogin?: boolean;
}

@State<AuthStateModel>({
  name: 'auth',
  defaults: {
    isAuthenticated: false,
    token: undefined,
    user: undefined,
    userId: undefined,
    forCart: undefined,
    redirectAfterLogin: false,
  }
})
@Injectable()
export class AuthState {
  constructor(
    private authService: AuthService,
    private notificationService: NotificationService,
  ) {
  }

  @Selector()
  static getToken(state: AuthStateModel): any {
    return state.token;
  }

  @Selector()
  static isAuthenticated(state: AuthStateModel): boolean {
    return !!state.token;
  }

  @Selector()
  static getForCart(state: AuthStateModel): any {
    return state.forCart;
  }

  @Selector()
  static getProfile(state: AuthStateModel): any {
    return state.user;
  }

  @Action(Login)
  login({patchState, dispatch, getState}: StateContext<AuthStateModel>, {payload}: Login): any {

    return this.authService.login(payload).pipe(
      tap((result: IAuth) => {
        patchState({
          token: result.token,
          user: result.user,
          forCart: result.user.id,
          isAuthenticated: true
        });
        dispatch(new Auth(result));
      })
    );
  }

  @Action(Recovery)
  recovery({patchState, dispatch, getState}: StateContext<AuthStateModel>, {payload}: Recovery): any {

    return this.authService.recovery(payload).pipe(
      tap((result: any) => {
        this.notificationService.success('გთხოვთ პაროლი განახლებულია', 'პაროლის განახლება');
        dispatch(new Navigate(['/auth'], undefined, {fragment: 'login'}));
      })
    );
  }

  @Action(RefreshToken)
  refreshToken({patchState, dispatch, getState}: StateContext<AuthStateModel>, {payload}: any): any {
    const {refreshToken}: any = getState().token;
    return this.authService.refreshToken(refreshToken).pipe(
      tap((result: IAuth) => {
        patchState({
          token: result.token,
          user: result.user
        });
      })
    );
  }

  @Action(Auth)
  auth({getState, patchState, dispatch}: StateContext<AuthStateModel>, {payload}: Auth): void {
    const redirectAfterLogin = getState().redirectAfterLogin;
    if (redirectAfterLogin) {
      dispatch(new Navigate(['/checkout/preview']));
      dispatch(new Redirect(false));
      return;
    }
    dispatch(new Navigate(['/']));
  }

  @Action(Redirect)
  setRedirect({getState, patchState, dispatch}: StateContext<AuthStateModel>, {payload}: Redirect): any {
    patchState({
      redirectAfterLogin: payload
    });
  }

  @Action(SetUserId)
  setUserId({getState, patchState, dispatch}: StateContext<AuthStateModel>, {userId}: SetUserId): any {
    const {user} = getState();
    if (user && user.id) {
      patchState({
        forCart: user.id
      });
    } else {
      patchState({
        forCart: userId
      });
    }
  }


  @Action(Logout)
  logout({patchState, dispatch}: StateContext<AuthStateModel>): void {
    patchState({
      token: undefined,
      user: undefined,
      redirectAfterLogin: undefined,
      isAuthenticated: false
    });
    window.location.reload();
  }

  @Action(LoginRedirect)
  onLoginRedirect({setState, dispatch}: StateContext<AuthStateModel>): void {
    dispatch(new Navigate(['/']));
  }

  @Action(Register)
  register({patchState, dispatch}: StateContext<AuthStateModel>, {payload}: Register): any {
    return this.authService.register(payload).pipe(
      tap((result: any) => {
        patchState({
          token: null,
          user: result
        });
        this.notificationService.success('გთხოვთ გაიაროთ რეგისტრაცია', 'რეგისტრაცია');
        dispatch(new Navigate(['/auth'], undefined, {fragment: 'login'}));
      })
    );
  }
}
