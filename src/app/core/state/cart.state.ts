import {Action, Selector, State, StateContext, Store} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {CartService} from '../services/cart.service';
import {tap} from 'rxjs/operators';
import {AddToCart, DeleteCartItem, GetCartItems, OrderCreate, SetCartShipping, UpdateCartItem} from './cart.actions';
import {CartItem} from '../interfaces/cart';
import {NotificationService} from '../services/notification.service';
import {AuthState} from './auth.state';
import {Address} from '../model/address';

export class CartStateModel {
  cart: CartItem[] | any;
  shippingAddress: Address | undefined;
  qrCodeView: string | undefined;
  tbcCardRedirect?: any;
}


@State<CartStateModel>({
  name: 'cart',
  defaults: {
    cart: [],
    shippingAddress: undefined,
    qrCodeView: undefined,
    tbcCardRedirect: undefined,
  }
})
@Injectable()
export class CartState {


  constructor(
    private cartService: CartService,
    private notificationService: NotificationService,
    private store: Store,
  ) {
  }


  @Selector()
  static getCart(state: CartStateModel): CartItem[] {
    return state.cart;
  }

  @Selector()
  static getCartItemQuantity(state: CartStateModel): number {
    return state.cart.reduce((prev: any, curr: { quantity: any; }) => {
      return prev + curr.quantity;
    }, 0);
  }

  @Selector()
  static getCartItemSumPrice(state: CartStateModel): number {
    return state.cart.reduce((prev: number, curr: { quantity: number; product: { price: number; }; }) => {
      return prev + (curr.quantity * curr.product.price);
    }, 0);
  }

  @Selector()
  static getTbcCardRedirect(state: CartStateModel): number {
    return state.tbcCardRedirect;
  }


  @Action(AddToCart)
  async addToCart({patchState, dispatch, getState}: StateContext<CartStateModel>, {payload}: AddToCart): Promise<any> {
    const {cart} = getState();
    const findCart = cart.find((i: { product: { id: any; }; }) => i.product.id === payload.product.id);
    const findIndex = cart.findIndex((i: { product: { id: any; }; }) => i.product.id === payload.product.id);


    const isAuth = this.store.selectSnapshot(AuthState.isAuthenticated);
    const forCart = this.store.selectSnapshot(AuthState.getForCart);
    const params = {
      ...payload,
      productId: payload.product.id,
      forCart
    };
    if (isAuth) {
      this.cartService.addToCart(params).subscribe((result: any) => {
        if (findIndex < 0) {
          patchState({
            cart: [
              ...cart,
              payload
            ]
          });
        } else {
          patchState({
            cart: [
              ...cart.slice(0, findIndex),
              {
                quantity: payload.quantity + findCart.quantity,
                size: payload.size,
                product: payload.product,
                attribute: payload.attribute
              },
              ...cart.slice(findIndex + 1),
            ]
          });
        }
        this.notificationService.success(payload.product.name, 'კალათაში დამატება');
      });
    } else {
      if (findIndex < 0) {
        patchState({
          cart: [
            ...cart,
            payload
          ]
        });
      } else {
        patchState({
          cart: [
            ...cart.slice(0, findIndex),
            {
              quantity: payload.quantity + findCart.quantity,
              size: payload.size,
              product: payload.product,
              attribute: payload.attribute,
            },
            ...cart.slice(findIndex + 1),
          ]
        });
      }
      this.notificationService.success(payload.product.name, 'კალათაში დამატება');
    }

  }


  @Action(DeleteCartItem)
  deleteCartItem({patchState, dispatch, getState}: StateContext<CartStateModel>, {payload}: DeleteCartItem): any {
    console.log(payload);
    const {cart} = getState();
    const findIndex = cart.findIndex((i: any) => i.product.id === payload.product.id);
    console.log(findIndex);
    patchState({
      cart: [
        ...cart.slice(0, findIndex),
        ...cart.slice(findIndex + 1),
      ]
    });
  }

  @Action(UpdateCartItem)
  updateCartItem({patchState, dispatch, getState}: StateContext<CartStateModel>, {payload}: UpdateCartItem): any {
    const {cart} = getState();
    const findCart = cart.find((i: { product: { id: any; }; }) => i.product.id === payload.product.id);
    const findIndex = cart.findIndex((i: { product: { id: any; }; }) => i.product.id === payload.product.id);
    patchState({
      cart: [
        ...cart.slice(0, findIndex),
        {
          ...findCart,
          quantity: payload.quantity
        },
        ...cart.slice(findIndex + 1),
      ]
    });
  }


  @Action(GetCartItems)
  getCartItems({patchState}: StateContext<CartStateModel>) {

    return this.cartService.getCartItems().pipe(
      tap((result: any[]) => {
        patchState({
          cart: result
        });
      })
    );
  }


  @Action(OrderCreate)
  orderCreate({patchState, getState, dispatch}: StateContext<CartStateModel>, {payload}: OrderCreate): any {
    const state = getState();
    const sum = state.cart.reduce((prev: number, curr: { quantity: number; product: { price: number; }; }) => {
      return prev + (curr.quantity * curr.product.price);
    }, 0);
    const params = {
      ...state,
      sumPrice: sum,
      providerId: payload.providerId
    };
    return this.cartService.orderCreate(params).pipe(
      tap((result: any) => {
        console.log(result);
        if (result) {
          if (payload.providerId === 1) {
            patchState({
              qrCodeView: result.qrCodeViewUrl
            });
            const link = document.createElement('a');
            link.target = '_blank';
            link.href = result.redirectUrl;
            link.setAttribute('visibility', 'hidden');
            link.click();
          }
          if (payload.providerId === 2) {
            patchState({
              tbcCardRedirect: result
            });
            const form = document.createElement('form');
            form.setAttribute('action', result.url);
            form.setAttribute('method', 'POST');

            const input = document.createElement('input');
            input.setAttribute('type', 'hidden');
            input.setAttribute('name', 'trans_id');
            input.setAttribute('value', result.transId);
            form.append(input);

            const button = document.createElement('button');
            button.setAttribute('type', 'submit');

            form.append(button);
            document.body.append(form);
            console.log(form);
            button.click();
          }
        }
        // location.open(result.data.redirectUrl, '_blank');
        // window.open(result.redirectUrl, '_blank');
        // this.location.prepareExternalUrl(result.data.redirectUrl);
      })
    );
  }
}


