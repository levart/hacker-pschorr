import {Action, Selector, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {tap} from 'rxjs/operators';
import {GlobalAccess, ShippingRatePercent} from './common.actions';
import {CartService} from '../services/cart.service';

export class CommonStateModel {
  globalOrderAccess: any;
  shippingRatePercent: any;
}

@State<CommonStateModel>({
  name: 'common',
  defaults: {
    globalOrderAccess: false,
    shippingRatePercent: undefined
  }
})
@Injectable()
export class CommonState {
  constructor(
    private cartService: CartService
  ) {
  }


  @Selector()
  static getGlobalAccess(state: CommonStateModel): any {
    return state.globalOrderAccess;
  }


  @Selector()
  static getShippingRatePercent(state: CommonStateModel): any {
    return state.shippingRatePercent;
  }

  @Action(GlobalAccess)
  globalAccess({patchState, dispatch}: StateContext<CommonStateModel>): any {
    // Use a language
    return this.cartService.getOrderAccess()
      .pipe(
        tap(res => {
          if (res && res.optionValue) {
            patchState({
              globalOrderAccess: JSON.parse(res.optionValue)
            });
          }
        })
      );
  }

  @Action(ShippingRatePercent)
  shippingRatePercent({patchState, dispatch}: StateContext<CommonStateModel>): any {
    return this.cartService.shippingRatePercent()
      .pipe(
        tap(res => {
          if (res) {
            patchState({
              shippingRatePercent: +res.optionValue
            });
          }
        })
      );
  }
}
