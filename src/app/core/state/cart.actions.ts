import {CartItem} from '../interfaces/cart';


export class AddToCart {
  static readonly type = '[CART] add to cart ';

  constructor(public payload: { product: any; quantity: number; size: string, attribute: any}) {
  }
}


export class DeleteCartItem {
  static readonly type = '[CART] delete to cart item';

  constructor(public payload: CartItem) {
  }
}


export class UpdateCartItem {
  static readonly type = '[CART] update to cart item';

  constructor(public payload: CartItem) {
  }
}

export class GetCartItems {
  static readonly type = '[CART] get cart items ';

}


export class SetCartShipping {
  static readonly type = '[CART] set cart shipping address ';

}

export class OrderCreate {
  static readonly type = '[CART] order create ';
  constructor(public payload: { providerId: number }) {
  }
}
