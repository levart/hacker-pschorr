
export class GlobalAccess {
  static readonly type = '[COMMON] get order global access';
}

export class ShippingRatePercent {
  static readonly type = '[COMMON] get shippingRatePercent';
}

